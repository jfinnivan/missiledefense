package com.example.jjfinnivan.androidgame;

/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	Ship class

*/

public class Ship {
	int x, y, speed;
    boolean visible;
    Bitmap theShip;
    int index = 0;
    Rect r;
    boolean boomed;
    int score;

    Ship(Bitmap s, int speed, int score) {
        theShip = s;
        this.speed = speed;
        visible = false;
        this.score = score;
        r = new Rect();
        boomed = false;
    }

    // set the x,y position of the ship
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }

    // draw the ship.  if it's "boomed", i.e, it's been exploded,
    // we draw the fireball instead of the ship
    public void draw(Canvas c, Paint p) {
        if (visible) {
            if (boomed) {
                c.drawBitmap(boom, x, y, p);
            } else {
                r.set(x, y, x + theShip.getWidth(), y + theShip.getHeight());
                //c.drawRect(r, p);
                c.drawBitmap(theShip, x, y, p);
            }
        }
    }

    // Explode the ship
    public void explode(Canvas c, Paint p) {
        c.drawBitmap(boom, x, y, p);
        boomed = true;
    }

    // Move the ship
    public void move() {
        x += speed;
    }

    public void remove() {
        if (boomed == false)
            visible = false;
    }

    public int getX() {
        return x;
    }

    // get the bounding rectangle
    public Rect getRect() {
        if (boomed)
            return null;
        else
            return r;

    }

    // The point value of this ship type
    public int getScore() {
        return score;
    }
}

