package com.example.jjfinnivan.androidgame;

/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	Missile class

*/

public class Missile {
	int x, y, speed;
    boolean visible;
    Bitmap theImage;
    Rect r;
    int index = 0;

    Missile() {
        theImage = missilePic;
        this.speed = missileSpeed;
        visible = false;
        r = new Rect();
    }

    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
        visible = true;
    }

    public void draw(Canvas c, Paint p) {
        if (visible) {
            c.drawBitmap(theImage, x, y, p);
            r.set(x, y, x + theImage.getWidth(), y + theImage.getHeight());
            //c.drawRect(r, butPaint);
        }
    }

    // move the missile.  We are moving up the screen, so we subtract
    public void move() {
        y -= speed;
    }

    public void remove() {
        visible = false;
    }

    public int getY() {
        return y;
    }

    public Rect getRect() {
        return r;
    }
}

