package com.example.jjfinnivan.androidgame;

import android.os.CountDownTimer;
/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	MyCountDownTimer class

*/
public class MyCountDownTimer extends CountDownTimer {

    public MyCountDownTimer(long startTime, long interval) {
        super(startTime, interval);
    }

    @Override
    public void onFinish() {
        timerFinished = true;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        MyTime = "" + (millisUntilFinished / 1000);
    }
}



