package com.example.jjfinnivan.androidgame;

/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	Button class

*/
// MyButton class
// This class will display a button with text
// It provides a method that indicates if a passed in x,y coordinate falls within the button boundaries
public class MyButton {
	int x, y;
    int width;
    int height;
    int color;
    String t;
    Bitmap b;
    Rect r;

    MyButton() {
    }

    public void setTextButton(int x, int y, int width, int height, int color, String t) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;
    this.t = t;
    }

    public int getLeft() {
    return x;
    }

    public int getTop() {
    return y;
    }

    public int getBottom() {
    return y + height;
    }

    public int getRight() {
    return x + width;
    }

    // Returns true of the x,y coordinate is within the button boundaries
    public boolean contains(int x, int y) {
    if ((x >= getLeft()) &&
    	(x <= getRight()) &&
        (y >= getTop()) &&
        (y <= getBottom())) {
        return true;
    } else
        return false;
    }


    public void drawButton(Canvas c, Paint p) {
    p.setTextSize(height / 2);
    p.setColor(color);
    r = new Rect(x, y, x + width, y + height);
    r.set(x, y, x + width, y + height);
    c.drawRect(r, p);
    p.setColor(Color.BLACK);
    p.setTextAlign(Paint.Align.CENTER);
    c.drawText(t, x + (width / 2), y + (height * 2) / 3, p);

    }

    public Rect getRect() {
    return r;
    }
}



