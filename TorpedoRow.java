package com.example.jjfinnivan.androidgame;

/*

    Author: Joe Finnivan
    Date:    9/19/2015
    Class:     Software Capstone CSC478C

	missileRow class

*/

// This is the row of all missiles
public class MissileRow {
	int start, end, x;
    Missile[] missiles = new Missile[21];
    int MaxMissiles = 20;
    int lastMissileIndex;
    int missileCount;


    MissileRow() {
        missileCount = 0;
        lastMissileIndex = 0;
    }

    public void setLocation(int start, int end, int x) {
        this.start = start;
        this.end = end;
        this.x = x;
    }

    // draw all missiles in the list
    public void drawRow(Canvas c, Paint p) {
        c.drawBitmap(gun, x - (gun.getWidth() / 4), start, p);

        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] != null) {
                missiles[i].draw(c, p);
            }
        }
    }

    // add a new missile
    public void addmissile() {
        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] == null) {

                missiles[i] = new missile();
                missiles[i].index = i;
                missiles[i].setPos(x, start);
                missileCount++;
                return;
            }
        }
    }

    // move each missile.  If one falls off the end, remove it
    public void updatemissiles() {
        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] != null) {
                missiles[i].move();
                if (missiles[i].getY() <= end) {
                    missiles[i].remove();
                    missiles[i] = null;
                    missileCount--;
                }
            }
        }
    }

    // Check to see if we have collided with a ship
    public boolean checkForCollision(Rect sr) {
        for (int i = 0; i < MaxMissiles; i++) {
            if (missiles[i] != null) {
                if (Rect.intersects(sr, missiles[i].getRect())) {
                    missiles[i].remove();
                    missiles[i] = null;
                    missileCount--;
                    return true;
                }
            }
        }
        return false;
    }

    // Clear all missiles
    public void clearmissiles() {
        for (int i = 0; i < MaxMissiles; i++) {
            missiles[i] = null;
        }
    }

}



